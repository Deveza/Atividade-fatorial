package fatorial.atividadeFatorial;

public class Fatorial {

	public static void main(String[] args) {
		int res = calculaFat(5);
		System.out.println("Fatorial = " + res);
	}

	static int calculaFat(int n) {
		return n == 0 ? 1 : n * calculaFat(n - 1);
	}
}
