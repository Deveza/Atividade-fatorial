package fatorial.atividadeFatorial;
import fatorial.atividadeFatorial.Fatorial;

import static org.junit.Assert.assertEquals;

import org.junit.Test;



public class TestFat{	
	@Test
	public void testFatorial() {
		int actual = Fatorial.calculaFat(5);
		int expected = 120;
		assertEquals(expected, actual);
		
	}

}
